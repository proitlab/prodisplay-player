<?php
  session_start();
  if (!$_SESSION['signed_in']) {
    $_SESSION['flash_error'] = "Please sign in";
    header("Location: /login.php");
    exit; // IMPORTANT: Be sure to exit here!
  }
?>
<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>ProDisplay Dashboard</title>
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="stylesheet" type="text/css" href="css/abeezee.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/txtlimit.js"></script>
<!--[if lt IE 9]>
  <script src="js/html5.js"></script>
<![endif]-->
</head>
<body>
  <div id="w">

<h1> Upload Video</h1>
<h3>Format must be mp4 with 720 resolution and max size 100MB</h3>
<a href="/"><h3>Main Page</h3></a>

<?php

for ($i = 1; $i <= 10; $i++) {
echo "<hr />\n";
echo "<strong>Video #" . $i . " </strong>";
$filename="/opt/proit/assets/videolist/" . $i . ".mp4";
if (file_exists($filename)) { 
echo "<form action=\"video_play.php\" method=\"post\">\n";
echo "<p id=\"section\">\n";
echo "Filename: <strong>" . $i . ".mp4</strong>\n"; 
echo "<input type=\"hidden\" name=\"videoname\" value=\"" . $i . "\" />\n";
echo "<input type=\"submit\" value=\"Play\" />\n";
echo "</p>\n";
echo "</form>\n";
}

echo "<form action=\"video_upload.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
echo "<p id=\"section\">\n";
echo "<input type=\"file\" name=\"video\" />\n";
echo "<input type=\"hidden\" name=\"videoname\" value=\"" . $i . "\" />\n";
echo "<input type=\"submit\" value=\"Upload\" />\n";
echo "</p>\n";
echo "</form>\n";
//echo "<hr />\n";
}
?>

  </div>
</body>
</html>
