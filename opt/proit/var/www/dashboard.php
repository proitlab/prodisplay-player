<?php
  session_start();
  if (!$_SESSION['signed_in']) {
    $_SESSION['flash_error'] = "Please sign in";
    header("Location: /login.php");
    exit; // IMPORTANT: Be sure to exit here!
  }
?>
<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>ProDisplay Dashboard</title>
  <link rel="stylesheet" type="text/css" href="studio/css/styles.css">
  <link rel="stylesheet" type="text/css" href="studio/css/abeezee.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/txtlimit.js"></script>
<!--[if lt IE 9]>
  <script src="js/html5.js"></script>
<![endif]-->
</head>
<body>
  <div id="w">

<table>
<tr>
<td>
<a href="http://www.proit.co.id"><img height="60" src="images/proit-logo-transparent-square.png" /></a>
</td>
<td style="vertical-align:middle">
<h1>ProDisplay</h1>
<h3>Customize Your Presentation</h3>
</td>
</tr>
</table>

<!--
<a href="logo_form.php"><h3>Logo</h3></a>
<a href="slide_form.php"><h3>Slide</h3></a>
<a href="video_form.php"><h3>Video</h3></a>
<a href="text_form.php"><h3>Running Text</h3></a>
-->
<br />
<br />
<a href="studio/"><h3>Manage your Assets with ProDisplay Studio</h3></a>
<br />
<a href="orientation.php"><h3>Display Orientation</h3></a>
<br />
<a href="wifi_form.php"><h3>Setup Wifi</h3></a>
<br />
<a href="logout.php"><h3>Log Out</h3></a>

<?php include("footer.php"); ?>

  </div>
</body>
</html>
