<?php

  // clear out any existing session that may exist
  session_start();
  session_destroy();

  header("Location: index.php");
?>
