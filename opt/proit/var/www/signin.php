<?php
  $password_file = "assets/password";

  function check_pass($login, $password) {
    global $password_file;
    if(!$fh = fopen($password_file, "r")) {die("<P>Could Not Open Password File");}
    $match = 0;
    $password = md5($password);
    while(!feof($fh)) {
      $line = fgets($fh, 4096);
      $user_pass = explode(":", $line);
      if($user_pass[0] == $login) {
        if(rtrim($user_pass[1]) == $password) {
          $match = 1;
          break;
        }
      }
    }
    if($match) {
      return 1;
    } else {
     return 0;
    }
  }

  $username = $_POST["username"];
  $password_post = $_POST['password'];

  $row = check_pass($username,$password_post);

  // clear out any existing session that may exist
  session_start();
  session_destroy();
  session_start();

  if ($row) {
    $_SESSION['signed_in'] = true;
    $_SESSION['username'] = $username;
    header("Location: dashboard.php");
  } else {
    $_SESSION['flash_error'] = "Invalid username or password";
    $_SESSION['signed_in'] = false;
    $_SESSION['username'] = null;
    header("Location: login.php");
 }
?>
