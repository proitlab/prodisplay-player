<?php
  session_start();
  if (!$_SESSION['signed_in']) {
    $_SESSION['flash_error'] = "Please sign in";
    header("Location: /login.php");
    exit; // IMPORTANT: Be sure to exit here!
  }
?>
<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>ProDisplay Dashboard</title>
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="stylesheet" type="text/css" href="css/abeezee.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/txtlimit.js"></script>
<!--[if lt IE 9]>
  <script src="js/html5.js"></script>
<![endif]-->
</head>
<body>
  <div id="w">

<h1>Upload Logo</h1>
<h3>Format must be JPG</h3>
<a href="/"><h3>Main Page</h3></a>

<form action="logo_upload.php" method="post" enctype="multipart/form-data">
<p>
<?php echo "<img  src='assets/logo.jpg?" . filemtime('assets/logo.jpg') . "' width=100 height=77  />"; ?>
Logo <input type="file" name="pictures" />
<input type="hidden" name="logo" value="logo" />
<input type="submit" value="Upload" />
</p>
</form>

  </div>
</body>
</html>
