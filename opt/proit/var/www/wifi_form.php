<?php
  session_start();
  if (!$_SESSION['signed_in']) {
    $_SESSION['flash_error'] = "Please sign in";
    header("Location: /login.php");
    exit; // IMPORTANT: Be sure to exit here!
  }
?>

<?php
ob_start(); 
$status = system('sudo /opt/proit/bin/ssid_password.ds > /tmp/ssid_password.txt');
$file_handle = fopen("/tmp/ssid_password.txt", "r");
$line_of_text = fgets($file_handle);
$parts = explode(' ', $line_of_text);
fclose($file_handle);
$status = system('rm -fr /tmp/ssid_password.txt');
ob_end_clean();
?>


<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>ProDisplay Dashboard</title>
  <link rel="stylesheet" type="text/css" href="studio/css/styles.css">
  <link rel="stylesheet" type="text/css" href="studio/css/abeezee.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/txtlimit.js"></script>
<!--[if lt IE 9]>
  <script src="js/html5.js"></script>
<![endif]-->
</head>

<body>
  <div id="w">
<table>
<tr>
<td>
<a href="http://www.proit.co.id"><img height="60" src="images/proit-logo-transparent-square.png" /></a>
</td>
<td style="vertical-align:middle">
<h1>Setup Wifi</h1>
<h3>Setup your wifi connection</h3>
</td>
</tr>
</table>
<br />
<br />

	<a href="/"><h3>Main Page</h3></a>
  	
	<form name="RunningTextForm" method="POST" action="wifi_setup.php">
	<br />
	SSID: <input name="ssid" type="text" value="<?php echo $parts[0]; ?>">
	<br />
	Password: <input name="wifipass" type="text" value="<?php echo $parts[1]; ?>">
	<br />
	<br />
	<input name="Submit" type="submit" value="Submit">
	</form>
	<br />
	<p><strong>Note:</strong></p>
	<p>After you click "Submit", press "Reload" or "Refresh" a couple times while checking log message below</p>
	<p>Click "Reload" or "Refresh" until you get IP address</p>
	<br />
	<br />
	<hr />
	<?php $status = system('ifconfig wlan0'); ?>
	<br />
	<?php $status = system('iwconfig wlan0'); ?>
	<hr />

<?php include("footer.php"); ?>
  </div>
</body>
</html>
