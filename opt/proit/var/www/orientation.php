<?php
  session_start();
  if (!$_SESSION['signed_in']) {
    $_SESSION['flash_error'] = "Please sign in";
    header("Location: /login.php");
    exit; // IMPORTANT: Be sure to exit here!
  }
?>

<?php
	$CONFIG=parse_ini_file("/boot/config.txt");
	$orientation = $CONFIG['display_rotate'];
?>


<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>ProDisplay Dashboard</title>
  <link rel="stylesheet" type="text/css" href="studio/css/styles.css">
  <link rel="stylesheet" type="text/css" href="studio/css/abeezee.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/txtlimit.js"></script>
<!--[if lt IE 9]>
  <script src="js/html5.js"></script>
<![endif]-->
</head>

<body>
  <div id="w">
<table>
<tr>
<td>
<a href="http://www.proit.co.id"><img height="60" src="images/proit-logo-transparent-square.png" /></a>
</td>
<td style="vertical-align:middle">
<h1>Display Orientation</h1>
<h3>Landscape/Horizontal or Potrait/Vertical</h3>
</td>
</tr>
</table>
<br />
<br />

	<a href="/"><h3>Main Page</h3></a>
<?php
	if(isset($_POST['Submit'])) 
		$orientation = $_POST['orientation'];
?>
  	
	<br />
	<br />
	<form name="Orientation" method="POST" action="orientation.php">
	<label>Display Orientation</label>
        <select name="orientation">
        <option value="0" <?php if($orientation == "0")  echo "selected"; ?>>Landscape/Horizontal</option>
        <option value="1" <?php if($orientation == "1")  echo "selected"; ?>>Potrait/Vertical</option>
        </select>
	<br />
	<br />
	<input name="Submit" type="submit" value="Change">
	</form>
	<br />
	<br />
	<p><strong>Note:</strong></p>
<?php
	if(isset($_POST['Submit'])) {
		//$CONFIG=parse_ini_file("/boot/config.txt");
		if($CONFIG['display_rotate'] != $_POST['orientation'])	{
			if($_POST['orientation'] == 1)
				$command = "sudo /opt/proit/bin/goPotrait.ds";
			else
				$command = "sudo /opt/proit/bin/goLandscape.ds";
			ob_start();
			$result=system($command);
			$result=system("sudo /sbin/reboot");
			ob_end_clean();
		}
		echo "<p>Rebooting...</p>";
	} else {
		echo "<p>System reboot is required</p>";
	}
?>

<?php include("footer.php"); ?>
  </div>
</body>
</html>
