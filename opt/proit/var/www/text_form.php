<?php
  session_start();
  if (!$_SESSION['signed_in']) {
    $_SESSION['flash_error'] = "Please sign in";
    header("Location: /login.php");
    exit; // IMPORTANT: Be sure to exit here!
  }
?>
<?php 
define("FILE_NAME", "assets/feed.input");
function Read() {
	echo @file_get_contents(FILE_NAME);
    //$file = "uploads/feed.input";
    //$fp = fopen($file, "r");
    //while(!feof($fp)) {
    //    $data = fgets($fp, filesize($file));
    //    echo $data;
   // }
    //fclose($fp);
}

?>

<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>ProDisplay Dashboard</title>
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="stylesheet" type="text/css" href="css/abeezee.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/txtlimit.js"></script>
<!--[if lt IE 9]>
  <script src="js/html5.js"></script>
<![endif]-->
</head>

<body>
  <div id="w">
  	<h1>Running Text Editor</h1>
  	<h3 class="desc">Text is limited up to 256 characters</h3>
	<a href="/"><h3>Main Page</h3></a>
  	
  	<p id="counter"><span>0</span> characters</p>
  	
	<form name="RunningTextForm" method="POST" action="text_upload.php">
  	<textarea name="text" id="text" class="txt" tabindex="1"><?php Read(); ?></textarea>
	<br />
	<input name="Submit" type="submit" value="Submit">
	</form>
  </div>
</body>
</html>
