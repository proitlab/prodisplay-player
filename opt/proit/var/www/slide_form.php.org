<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Textarea Character Limit with jQuery</title>
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="stylesheet" type="text/css" href="css/abeezee.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/txtlimit.js"></script>
<!--[if lt IE 9]>
  <script src="js/html5.js"></script>
<![endif]-->
</head>
<body>
  <div id="w">

<h1>Upload Slides</h1>
<h3>Format must be JPG</h3>
<hr />
<form action="slide_upload.php" method="post" enctype="multipart/form-data">
<p>
<?php echo "<img  src='assets/Slide1.jpg?" . filemtime('assets/Slide1.jpg') . "' width=100 height=77  />"; ?>
Slide #1 <input type="file" name="pictures" />
<input type="hidden" name="slidename" value="Slide1" />
<input type="submit" value="Upload" />
</p>
</form>
<hr />
<form action="slide_upload.php" method="post" enctype="multipart/form-data">
<p>
<?php echo "<img  src='assets/Slide2.jpg?" . filemtime('assets/Slide2.jpg') . "' width=100 height=77  />"; ?>
Slide #2 <input type="file" name="pictures" />
<input type="hidden" name="slidename" value="Slide2" />
<input type="submit" value="Upload" />
</p>
</form>
<hr />
<form action="slide_upload.php" method="post" enctype="multipart/form-data">
<p>
<?php echo "<img  src='assets/Slide3.jpg?" . filemtime('assets/Slide3.jpg') . "' width=100 height=77  />"; ?>
Slide #3 <input type="file" name="pictures" />
<input type="hidden" name="slidename" value="Slide3" />
<input type="submit" value="Upload" />
</p>
</form>
<hr />
<form action="slide_upload.php" method="post" enctype="multipart/form-data">
<p>
<?php echo "<img  src='assets/Slide4.jpg?" . filemtime('assets/Slide4.jpg') . "' width=100 height=77  />"; ?>
Slide #4 <input type="file" name="pictures" />
<input type="hidden" name="slidename" value="Slide4" />
<input type="submit" value="Upload" />
</p>
</form>
<hr />
<form action="slide_upload.php" method="post" enctype="multipart/form-data">
<p>
<?php echo "<img  src='assets/Slide5.jpg?" . filemtime('assets/Slide5.jpg') . "' width=100 height=77  />"; ?>
Slide #5 <input type="file" name="pictures" />
<input type="hidden" name="slidename" value="Slide5" />
<input type="submit" value="Upload" />
</p>
</form>

  </div>
</body>
</html>
